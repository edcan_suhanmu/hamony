package kr.edcan.lumihana.hamony.MainActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.roughike.bottombar.BottomBar;

import java.util.ArrayList;

import kr.edcan.lumihana.hamony.R;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerViewDecoration recyclerViewDecoration;
    private ArrayList<RecyclerData> arrayList;
    private BottomBar bottomBar;
    private Toolbar toolbar;
    private ImageView writeButton, searchButton;
    private StaggeredGridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.main_recyclerView);
//        bottomBar = (BottomBar) findViewById(R.id.main_bottom_bar);
        writeButton = (ImageView) findViewById(R.id.main_toolbar_write);
        searchButton = (ImageView) findViewById(R.id.main_toolbar_search);
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        toolbar.setTitle("Hamony");
        setSupportActionBar(toolbar);

        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerViewDecoration = new RecyclerViewDecoration(16);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(recyclerViewDecoration);

        arrayList = new ArrayList<>();
        arrayList = new ArrayList<>();
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData(null, null, RecyclerData.DATA_STATUS_DEBATE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", null, RecyclerData.DATA_STATUS_SYMPATHY, 5, 23));
        arrayList.add(new RecyclerData("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", null, RecyclerData.DATA_STATUS_VOTE, 5, 23));
        arrayList.add(new RecyclerData("HelloWorld", "김수한", RecyclerData.DATA_STATUS_VOTE, 5, 23));
        recyclerView.setAdapter(new RecyclerAdapter(getApplicationContext(), arrayList, R.layout.gridview_content_main));

//        bottomBar.setItemsFromMenu(R.menu.menu_main_bottom, new OnMenuTabClickListener() {
//            @Override
//            public void onMenuTabSelected(@IdRes int menuItemId) {
//                switch (menuItemId) {
//                    case R.id.main_bottom_recent: {
//                        Toast.makeText(getApplicationContext(), "1", Toast.LENGTH_SHORT).show();
//                        break;
//                    }
//                    case R.id.main_bottom_recent2: {
//                        Toast.makeText(getApplicationContext(), "2", Toast.LENGTH_SHORT).show();
//                        break;
//                    }
//                    case R.id.main_bottom_recent3: {
//                        Toast.makeText(getApplicationContext(), "3", Toast.LENGTH_SHORT).show();
//                        break;
//                    }
//                }
//            }
//
//            @Override
//            public void onMenuTabReSelected(@IdRes int menuItemId) {
//
//            }
//        });
//
//        bottomBar.mapColorForTab(0, "#FF0000");
//        bottomBar.mapColorForTab(1, "#00FF00");
//        bottomBar.mapColorForTab(2, "#0000FF");

        writeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

//        bottomBar.onSaveInstanceState(outState);
    }
}

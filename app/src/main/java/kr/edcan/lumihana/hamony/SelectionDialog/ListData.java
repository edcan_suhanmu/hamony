package kr.edcan.lumihana.hamony.SelectionDialog;

/**
 * Created by kimok_000 on 2016-05-11.
 */
public class ListData  {
    private String title;
    private String content;

    public ListData(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
